import numpy as np

# 创建数组
x = np.array([1.0, 2.0, 3.0])
print(x)
print(type(x))

# 数学运算
x = np.array([1.0, 2.0, 3.0])
y = np.array([2.0, 4.0, 6.0])

print(x + y)
print(x - y)
print(x * y)
print(x / y)

# 广播
x = np.array([1.0, 2.0, 3.0])
print(x / 2.0)

# n维数组创建、查看属性
A = np.array([[1, 2], [3, 4]])
print(A)
print(A.shape)
print(A.dtype)

# n维数组运算
B = np.array([[3, 0], [0, 6]])
print(A + B)
print(A * B)

# 广播
print(A)
print(A * 10)

# 访问元素
X = np.array([[51, 55], [14, 19], [0, 4]])
print(X)
print(X[0])  # 第0行
print(X[0][1])  # (0,1)的元素

# for遍历访问
for row in X:
    print(row)

# 使用数组访问
X = X.flatten()  # 将X转为一维数组
print(X)
print(X[np.array([0, 2, 4])])  # 获取索引为0、2、4的元素

# 获取满足一定条件的元素
print(X > 15)
print(X[X > 15])
